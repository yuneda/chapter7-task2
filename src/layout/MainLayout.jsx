import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "antd/dist/antd.css";
import "./index.css";
import { Layout } from "antd";
import MySider from "./Sider";
import Dashboard from "../components/Dashboard";
import UserDetail from "../components/UserDetail";

const { Header, Content, Footer } = Layout;

export default () => (
  <Layout>
    <MySider />
    <Layout>
      <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
      <Content style={{ margin: "24px 16px 0" }}>
        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
          <BrowserRouter>
            <Routes>
              <Route exact path="/" element={<Dashboard />} />
              <Route path="/:id" element={<UserDetail />} />
            </Routes>
          </BrowserRouter>
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>©2018 Created by Yuneda</Footer>
    </Layout>
  </Layout>
);
