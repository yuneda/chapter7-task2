import React, { useEffect, useState } from "react";
import MyCard from "./MyCard";
import { Row, Breadcrumb } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import Loading from "./Loading";

function Dashboard() {
  const [data, setData] = useState(null);
  useEffect(() => {
    setTimeout(() => {
      fetch("https://jsonplaceholder.typicode.com/users")
        .then((res) => res.json())
        .then((data) => {
          setData(data);
          console.log(data);
        });
    }, 1500);
  }, []);
  return (
    <div>
      <h1>DATA USER</h1>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
          <span>Dashboard</span>
        </Breadcrumb.Item>
      </Breadcrumb>
      {!data && <Loading />}
      <Row>
        {data &&
          data.map((user) => (
            <MyCard key={user.id} name={user.name} email={user.email} username={user.username} id={user.id} />
          ))}
      </Row>
    </div>
  );
}

export default Dashboard;
