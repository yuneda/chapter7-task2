import React from "react";
import "antd/dist/antd.css";
import { Link } from "react-router-dom";
import { Card, Col, Button } from "antd";

export default ({ name, email, username, id }) => {
  return (
    <Col span={8}>
      <Card title={name} style={{ width: 300 }}>
        <p>Username : {username}</p>
        <p>Email : {email}</p>
        <Link to={`/${id}`}>
          <Button type="primary">Detail</Button>
        </Link>
      </Card>
    </Col>
  );
};
