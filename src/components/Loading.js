import React from "react";
import "antd/dist/antd.css";
// import "./index.css";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

export default () => <Spin indicator={antIcon} />;
