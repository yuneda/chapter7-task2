import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Card, Button, Breadcrumb } from "antd";
import { HomeOutlined, UserOutlined } from "@ant-design/icons";
import Loading from "./Loading";

function UserDetail() {
  const { id } = useParams();
  const navigate = useNavigate();
  const [user, setUser] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      fetch("https://jsonplaceholder.typicode.com/users/" + id)
        .then((res) => res.json())
        .then((user) => {
          setUser(user);
          console.log(user);
        });
    }, 1000);
  });
  return (
    <div>
      <h1>USER DETAIL</h1>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
          <span>Dashboard</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item href="">
          <UserOutlined />
          <span>User Id : {id}</span>
        </Breadcrumb.Item>
      </Breadcrumb>
      {!user && <Loading />}
      {user && (
        <div>
          <Card style={{ width: 400 }}>
            <p>Username : {user.username}</p>
            <p>Email : {user.email}</p>
            <p>Phone : {user.phone}</p>
            <p>Website : {user.website}</p>
            <p>Company : {user.company.name}</p>
            <Button type="danger" onClick={() => navigate("/")}>
              Back
            </Button>
          </Card>
        </div>
      )}
    </div>
  );
}

export default UserDetail;
